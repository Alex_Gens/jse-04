package ru.kazakov.iteco.repository;

import java.util.List;

public abstract class AbstractRepository<T> {

    public abstract T findOne(String id);

    public abstract List<T> findAll();

    public abstract void merge(T entity);

    public abstract void persist(T entity);

    public abstract void remove(String id);

    public abstract void removeAll();

}
