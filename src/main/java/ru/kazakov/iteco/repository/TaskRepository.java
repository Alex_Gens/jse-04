package ru.kazakov.iteco.repository;

import ru.kazakov.iteco.entity.Task;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class TaskRepository extends AbstractRepository<Task> {

    Map<String, Task> entities = new LinkedHashMap<>();

    public String getName(String id) {
        return entities.get(id).getName();
    }

    public void setName(String name, String id) {
        entities.get(id).setName(name);
    }

    public String getProject(String id) {
        return entities.get(id).getProject();
    }

    public void setProject(String id, String projectId) {
        entities.get(id).setProject(projectId);
    }

    @Override
    public void merge(Task entity) {
        entities.merge(entity.getId(), entity, (v1, v2)
                -> {String newInfo = v1.getInfo() + v2.getInfo();
            entity.setInfo(newInfo);
            return entity;});
    }

    @Override
    public void persist(Task entity) {
        entities.putIfAbsent(entity.getId(), entity);
    }

    @Override
    public void remove(String id) {
        entities.remove(id);
    }

    public void remove(List<String> ids) {
        entities.entrySet().removeIf(entry
                -> ids.contains(entry.getValue().getId()));
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

    @Override
    public Task findOne(String id) {
        return entities.get(id);
    }

    @Override
    public List<Task> findAll() {
        List<Task> tasks = new ArrayList<>(entities.values());
        return tasks;
    }

    public List<Task> findAll(List<String> ids) {
        List<Task> values = (List<Task>) entities.values();
        values.removeIf(v -> !ids.contains(v.getId()));
        return values;
    }

    public boolean isEmpty() {
        return entities.isEmpty();
    }

    public boolean isEmpty(String id) {
        return entities.get(id).isEmpty();
    }

}
