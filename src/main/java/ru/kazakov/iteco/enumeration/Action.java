package ru.kazakov.iteco.enumeration;

public enum Action {

    CREATED,
    UPDATED,
    REMOVED,
    ADDED

}
