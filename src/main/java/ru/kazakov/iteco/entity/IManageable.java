package ru.kazakov.iteco.entity;

import java.util.Date;

public interface IManageable {

    public String getId();

    public Date getDateStart();

    public Date getDateFinish();

    public String getInfo();

    public void setInfo(String info);

}

