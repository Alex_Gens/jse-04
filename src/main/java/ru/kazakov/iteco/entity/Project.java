package ru.kazakov.iteco.entity;

import ru.kazakov.iteco.util.Format;
import java.util.Date;
import java.util.UUID;

public class Project implements IManageable {

    private final String id = UUID.randomUUID().toString();

    private String name = "";

    private String info = "";

    private Date dateStart;

    private Date dateFinish;

    public Project() {}

    public Project(String name) { this.name = name; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Date getDateStart() {
        return dateStart;
    }

    @Override
    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    @Override
    public String getInfo() {
        return info;
    }

    @Override
    public void setInfo(String info) {
        this.info = info;
    }

    public boolean isEmpty() {
        return Format.isEmpty(info);
    }

}
