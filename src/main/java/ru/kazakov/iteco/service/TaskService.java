package ru.kazakov.iteco.service;

import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.repository.TaskRepository;
import ru.kazakov.iteco.util.Format;
import java.util.List;

public class TaskService extends AbstractService<Task> {

    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public String getName(String id) throws Exception {
        if (Format.isEmpty(id)) {
            throw new Exception();
        }
        return taskRepository.getName(id);
    }

    public void setName(String name, String id) throws Exception {
        if (Format.isEmpty(name) || Format.isEmpty(id)) {
            throw new Exception();
        }
        taskRepository.setName(name, id);
    }

    public String getProject(String id) throws Exception {
        if (Format.isEmpty(id)) {
            throw new Exception();
        }
        return taskRepository.getProject(id);
    }

    public void setProject(String id, String projectId) throws Exception {
        if (Format.isEmpty(id) || Format.isEmpty(projectId)) {
            throw new Exception();
        }
        taskRepository.setProject(id, projectId);
    }

    @Override
    public void merge(Task entity) throws Exception {
        if (entity == null) {
            throw new Exception();
        }
        taskRepository.merge(entity);
    }

    @Override
    public void persist(Task entity) throws Exception {
        if (entity == null) {
            throw new Exception();
        }
        taskRepository.persist(entity);
    }

    public void persist(String name) throws Exception {
        if (Format.isEmpty(name)) {
            throw new Exception();
        }
        Task task = new Task(name);
        taskRepository.persist(task);
    }

    @Override
    public void remove(String id) throws Exception {
        if (Format.isEmpty(id)) {
            throw new Exception();
        }
        taskRepository.remove(id);
    }

    public void remove(List<String> ids) throws Exception {
        if (ids == null) {
            throw new Exception();
        }
        taskRepository.remove(ids);
    }

    @Override
    public void removeAll() {
        taskRepository.removeAll();
    }

    @Override
    public Task findOne(String id) throws Exception {
        if (Format.isEmpty(id)) {
            throw new Exception();
        }
        return taskRepository.findOne(id);
    }

    public Task findOne(String name, boolean isByName) throws Exception {
        if (Format.isEmpty(name)) {
            throw new Exception();
        }
        List<Task> list = taskRepository.findAll();
        return list.stream()
                .filter(v -> v.getName().equals(name))
                .findFirst().orElse(null);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public List<Task> findAll(List<String> ids) throws Exception {
        if (ids == null) {
            throw new Exception();
        }
        return taskRepository.findAll(ids);
    }

    @Override
    public boolean isEmpty() {
        return taskRepository.isEmpty();
    }

    public boolean isEmpty(String id) throws Exception {
        if (Format.isEmpty(id)) {
            throw new Exception();
        }
        return taskRepository.isEmpty(id);
    }

    public boolean contains(String name) throws Exception {
        if (Format.isEmpty(name)) {
            throw new Exception();
        }
        List<Task> list = taskRepository.findAll();
        return list.stream()
                .anyMatch(v -> v.getName().equals(name));
    }

}
