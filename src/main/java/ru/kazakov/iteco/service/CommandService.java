package ru.kazakov.iteco.service;

import ru.kazakov.iteco.enumeration.TaskManagerCommand;
import ru.kazakov.iteco.util.Format;

import java.util.Arrays;
import java.util.List;

public class CommandService {

    public TaskManagerCommand getCommand (String entered) throws Exception {
        if (Format.isEmpty(entered)) {
            throw new Exception();
        }
        List<TaskManagerCommand> commands = Arrays.asList(TaskManagerCommand.values());
        return commands.stream()
                .filter(com -> entered.equals(com.getValue()))
                .findFirst().orElse(TaskManagerCommand.NONE);
    }

}
