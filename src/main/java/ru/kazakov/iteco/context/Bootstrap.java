package ru.kazakov.iteco.context;

import static ru.kazakov.iteco.enumeration.Action.ADDED;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.Action;
import ru.kazakov.iteco.enumeration.EntityType;
import ru.kazakov.iteco.enumeration.TaskManagerCommand;
import ru.kazakov.iteco.util.ConsoleReader;
import ru.kazakov.iteco.repository.ProjectRepository;
import ru.kazakov.iteco.repository.TaskRepository;
import ru.kazakov.iteco.service.ProjectService;
import ru.kazakov.iteco.service.TaskService;
import ru.kazakov.iteco.service.CommandService;
import ru.kazakov.iteco.util.Format;
import ru.kazakov.iteco.view.ConsoleWriter;
import java.util.ArrayList;
import java.util.List;

public class Bootstrap {

    TaskRepository taskRepository = new TaskRepository();
    ProjectRepository projectRepository = new ProjectRepository();
    TaskService taskService = new TaskService(taskRepository);
    ProjectService projectService = new ProjectService(projectRepository ,taskRepository);
    Format format = new Format();
    CommandService commandService = new CommandService();
    ConsoleReader reader = new ConsoleReader();
    ConsoleWriter writer = new ConsoleWriter();

    public void init() throws Exception {

        String entered = "";
        while (true) {
            entered = reader.enterIgnoreEmpty();
            entered = format.getCommandForm(entered);
            TaskManagerCommand command = commandService.getCommand(entered);
            switch (command) {
                case HELP               : writer.showHelp();              break;
                case PROJECT_CREATE     : projectCreate();                break;
                case PROJECT_GET        : projectGet();                   break;
                case PROJECT_UPDATE     : projectUpdate();                break;
                case PROJECT_ADD_TASK   : projectAddTask();               break;
                case PROJECT_LIST_TASKS : projectListTasks();             break;
                case PROJECT_REMOVE     : projectRemove();                break;
                case PROJECT_LIST       : projectList();                  break;
                case PROJECT_CLEAR      : projectClear();                 break;
                case TASK_CREATE        : taskCreate();                   break;
                case TASK_GET           : taskGet();                      break;
                case TASK_UPDATE        : taskUpdate();                   break;
                case TASK_REMOVE        : taskRemove();                   break;
                case TASK_LIST          : taskList();                     break;
                case TASK_CLEAR         : taskClear();                    break;
                case EXIT               : exit();
                default                 : writer.showCommandNotExist();
            }
        }
    }

    private void projectCreate() throws Exception {
        EntityType entityType = EntityType.PROJECT;
        String name = writer.enterEntityName(entityType);
        if (projectService.contains(name)) {
            writer.showNotActionInBreakets(Action.CREATED);
            writer.showNameIsExist(entityType);
            writer.separateLines();
            return;
        }
        projectService.persist(name);
        writer.showActionInBreakets(Action.CREATED);
        writer.showSuccessfullyDone(entityType, Action.CREATED);
        writer.separateLines();
    }

    private void projectGet() throws Exception {
        EntityType entityType = EntityType.PROJECT;
        String name = writer.enterEntityName(entityType);
        if (!projectService.contains(name)) {
            writer.showNameNotExist(entityType);
            writer.separateLines();
            return;
        }
        Project project = projectService.findOne(name, true);
        if (projectService.isEmpty(project.getId())) {
            writer.showInfoIsEmpty(entityType);
            writer.separateLines();
            return;
        }
        writer.showNameInBreakets(entityType, name);
        writer.printInformation(project.getInfo());
        writer.separateLines();
    }

    private void projectUpdate() throws Exception {
        EntityType entityType = EntityType.PROJECT;
        String name = writer.enterEntityName(entityType);
        if (!projectService.contains(name)) {
            writer.showNameNotExist(entityType);
            writer.separateLines();
            return;
        }
        Project project = projectService.findOne(name, true);
        writer.showFinishAndSave();
        writer.showEnterInformation(entityType);
        String newInfo = reader.read();
        project.setInfo(newInfo);
        writer.showActionInBreakets(Action.UPDATED);
        writer.showSuccessfullyDone(entityType, Action.UPDATED);
        writer.separateLines();
    }

    private void projectRemove() throws Exception {
        EntityType entityType = EntityType.PROJECT;
        String name = writer.enterEntityName(entityType);
        if (!projectService.contains(name)) {
            writer.showNameNotExist(entityType);
            writer.separateLines();
            return;
        }
        Project project = projectService.findOne(name, true);
        List<String> taskIds = new ArrayList<>();
        for ( Task task : taskService.findAll()
             ) {
            if (task.getProject().equals(project.getId())) {
                taskIds.add(task.getId());
            }
        }
        taskService.remove(taskIds);
        projectService.remove(project.getId());
        writer.showActionInBreakets(Action.REMOVED);
        writer.showSuccessfullyDone(entityType, Action.REMOVED);
        writer.separateLines();
    }

    private void projectList() {
        EntityType entityType = EntityType.PROJECT;
        if (projectService.isEmpty()) {
            writer.showListIsEmpty(entityType);
            writer.separateLines();
            return;
        }
        int counter = 1;
        writer.showListInBreakets(entityType);
        List<Project> projects = projectService.findAll();
        for (Project project : projects) {
            writer.printListValue(counter, project.getName());
            counter++;
        }
        writer.separateLines();
    }

    private void projectAddTask() throws Exception {
        EntityType entityType = EntityType.PROJECT;
        String projectName = writer.enterEntityName(entityType);
        if (!projectService.contains(projectName)) {
            writer.showNameNotExist(entityType);
            writer.separateLines();
            return;
        }
        String taskName = writer.enterEntityName(EntityType.TASK);
        if (!taskService.contains(taskName)) {
            writer.showNameNotExist(EntityType.TASK);
            writer.separateLines();
            return;
        }
        Project project = projectService.findOne(projectName, true);
        Task task = taskService.findOne(taskName, true);
        if (task.getProject().equals(project.getId())) {
            writer.showNameIsAlreadyAdded(EntityType.TASK);
            writer.separateLines();
            return;
        }
        task.setProject(project.getId());
        writer.showSuccessfullyDone(EntityType.TASK, ADDED);
        writer.separateLines();
    }

    private void projectListTasks() throws Exception {
        EntityType entityType = EntityType.PROJECT;
        String projectName = writer.enterEntityName(entityType);
        if (!projectService.contains(projectName)) {
            writer.showNameNotExist(entityType);
            writer.separateLines();
            return;
        }
        Project project = projectService.findOne(projectName, true);
        List<String> taskIds = new ArrayList<>();
        for ( Task task : taskService.findAll()
        ) {
            if (task.getProject().equals(project.getId())) {
                taskIds.add(task.getId());
            }
        }
        if (taskIds.isEmpty()) {
            writer.showTaskListIsEmpty();
            writer.separateLines();
            return;
        }
        int counter = 1;
        writer.showListInBreakets(EntityType.TASK);
        for (String taskId : taskIds
        ) {
            writer.printListValue(counter, taskService.getName(taskId));
            counter++;
        }
        writer.separateLines();
    }

    private void projectClear() throws Exception {
        List<String> taskIds = new ArrayList<>();
        List<String> projectIds = new ArrayList<>();
        for ( Project project : projectService.findAll()) {
            projectIds.add(project.getId());
        }
        for ( Task task : taskService.findAll()) {
            if (projectIds.contains(task.getProject())) {
                taskIds.add(task.getId());
            }
        }
        taskService.remove(taskIds);
        projectService.removeAll();
        writer.showClear(EntityType.PROJECT);
    }

    private void taskCreate() throws Exception {
        EntityType entityType = EntityType.TASK;
        String name = writer.enterEntityName(entityType);
        if (taskService.contains(name)) {
            writer.showNotActionInBreakets(Action.CREATED);
            writer.showNameIsExist(entityType);
            writer.separateLines();
            return;
        }
        taskService.persist(name);
        writer.showActionInBreakets(Action.CREATED);
        writer.showSuccessfullyDone(entityType, Action.CREATED);
        writer.separateLines();
    }

    private void taskGet() throws Exception {
        EntityType entityType = EntityType.TASK;
        String name = writer.enterEntityName(entityType);
        if (!taskService.contains(name)) {
            writer.showNameNotExist(entityType);
            writer.separateLines();
            return;
        }
        Task task = taskService.findOne(name, true);
        if (taskService.isEmpty(task.getId())) {
            writer.showInfoIsEmpty(entityType);
            writer.separateLines();
            return;
        }
        writer.showNameInBreakets(entityType, name);
        writer.printInformation(task.getInfo());
        writer.separateLines();
    }

    private void taskUpdate() throws Exception {
        EntityType entityType = EntityType.TASK;
        String name = writer.enterEntityName(entityType);
        if (!taskService.contains(name)) {
            writer.showNameNotExist(entityType);
            writer.separateLines();
            return;
        }
        Task task = taskService.findOne(name, true);
        writer.showFinishAndSave();
        writer.showEnterInformation(entityType);
        String newInfo = reader.read();
        task.setInfo(newInfo);
        writer.showActionInBreakets(Action.UPDATED);
        writer.showSuccessfullyDone(entityType, Action.UPDATED);
        writer.separateLines();
    }

    private void taskRemove() throws Exception {
        EntityType entityType = EntityType.TASK;
        String name = writer.enterEntityName(entityType);
        if (!taskService.contains(name)) {
            writer.showNameNotExist(entityType);
            writer.separateLines();
            return;
        }
        Task task = taskService.findOne(name, true);
        taskService.remove(task.getId());
        writer.showActionInBreakets(Action.REMOVED);
        writer.showSuccessfullyDone(entityType, Action.REMOVED);
        writer.separateLines();
    }

    private void taskList() {
        EntityType entityType = EntityType.TASK;
        if (taskService.isEmpty()) {
            writer.showListIsEmpty(entityType);
            writer.separateLines();
            return;
        }
        int counter = 1;
        writer.showListInBreakets(entityType);
        List<Task> tasks = taskService.findAll();
        for (Task task : tasks) {
            writer.printListValue(counter, task.getName());
            counter++;
        }
        writer.separateLines();
    }

    private void taskClear() {
        taskService.removeAll();
        writer.showClear(EntityType.TASK);
    }

    private void exit() {
        new ConsoleWriter().showManagerClosed();
        System.exit(0);
    }

}
