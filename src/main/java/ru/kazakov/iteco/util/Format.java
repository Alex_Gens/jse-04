package ru.kazakov.iteco.util;

public class Format {

    public String firstUpperCase(String str) {
        String result = "";
        if (!isEmpty(str)) {
            result = str.substring(0, 1).toUpperCase() + str.substring(1);
        }
        return result;
    }

    public String getCommandForm(String entered) {
        return entered.trim().toLowerCase();
    }

    public static boolean isEmpty (String string) {
        return string == null || string.length() == 0;
    }

}
